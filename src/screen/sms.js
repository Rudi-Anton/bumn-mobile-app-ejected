import React, {Component} from 'react';
import { StyleSheet, Text, View, Button, Linking } from 'react-native';
// import Anchor from './components/detailLaporan'

export default class SMS extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Button
          title="Open URL with ReactNative.Linking"
          onPress={this._handleOpenWithLinking}
          style={styles.button}
        />
      </View>
    );
  }
  
  _handleOpenWithLinking = () => {
    Linking.openURL('sms:/open?addresses=&body=\Hello, Please follow the following url: \http://google.com');
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ecf0f1',
  },
  button: {
    marginVertical: 10,
  },
});